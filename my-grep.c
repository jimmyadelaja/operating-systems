#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]){
	
	//checks if there are no command line arguements
	if (argc == 1){
		printf("%s", "my-grep: searchterm [file ...] \n");
		exit(1);
	}
	
	//asks for file input if none is provided
	char readFile[1024];
	if (argc == 2){
		printf("%s", "Enter input file: \n");
		scanf("%1023[^\n]", readFile);
		char line[1024] ;
			if (strstr(readFile , argv[1] )!= NULL){
			   printf("%s",readFile);
			   printf("\n");
			}
	}
	
	else{
		//loop through all files palced in argv
		int loop = 2;
		while (loop < argc){  //loops through each of the files
			
			FILE *infile = fopen(argv[loop], "r");
			//checks if file cannot be opened
			if (infile == NULL){
				printf("my-grep: cannot open file: \n");
				exit(1);
			}
			else{
				char line[1024] ;
				while (fgets(line , sizeof(line) , infile )!= NULL){
					if (strstr(line , argv[1] )!= NULL){
					   printf("%s",line);
					}
				}	
			}
			//close file and increment loop so that next file in the arguements can be opened
			fclose(infile);
			loop++;
		}
	}

	//end progam
	return 0;
}

