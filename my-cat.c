#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]){
	
	//loop through all files palced in argv
	int loop = 2;
	while (loop < argc){
		char c;	
		FILE *infile = fopen(argv[loop], "r");
        //checks if file cannot be opened
		if (infile == NULL){
			printf("my-cat: cannot open file: ");
			exit(1);
		}
        //opens file and prints it out
		else{
			c = fgetc(infile);
			while (c != EOF){
				printf("%c", c);
				c = fgetc(infile);
			}
		}
        //close file and increment loop so that next file in the arguements can be opened
		fclose(infile);
		loop++;
	}
	//end progam
	return 0;
}
